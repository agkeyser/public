#Used to backup GPOs that are linked to a specific OU. Script has the ability to check a single OU or check recursively

$OU = #Enter OU DN here...Example 'OU=Departmental OUs,DC=net,DC=ucf,DC=edu'
$Recursive = 'y' #Change this to anything other than "y" to check a single OU. Not case sensitive.
$BackupPath = #Enter the path where you would like your backups saved...Example 'C:\GPOBackups'

if ($recursive -eq 'y'){
    $LinkedGPOs = Get-ADOrganizationalUnit -LDAPFilter '(name=*)' -SearchBase $OU -SearchScope Subtree | select -ExpandProperty LinkedGroupPolicyObjects            
    $GUIDRegex = "{[a-zA-Z0-9]{8}[-][a-zA-Z0-9]{4}[-][a-zA-Z0-9]{4}[-][a-zA-Z0-9]{4}[-][a-zA-Z0-9]{12}}"
        foreach($LinkedGPO in $LinkedGPOs){            
        $result = [Regex]::Match($LinkedGPO,$GUIDRegex);            
        if($result.Success){            
            $GPOGuid = $result.Value.TrimStart("{").TrimEnd("}")            
            Backup-GPO -Guid $GPOGuid -Path $BackupPath           
        }
    }
}

else{
    $LinkedGPOs = Get-ADOrganizationalUnit -Identity $OU | select -ExpandProperty LinkedGroupPolicyObjects            
    $GUIDRegex = "{[a-zA-Z0-9]{8}[-][a-zA-Z0-9]{4}[-][a-zA-Z0-9]{4}[-][a-zA-Z0-9]{4}[-][a-zA-Z0-9]{12}}"
        foreach($LinkedGPO in $LinkedGPOs){            
        $result = [Regex]::Match($LinkedGPO,$GUIDRegex);            
        if($result.Success){            
            $GPOGuid = $result.Value.TrimStart("{").TrimEnd("}")            
            Backup-GPO -Guid $GPOGuid -Path $BackupPath           
        }
    }
}